/**
  * GENÉRICOS 
  */

function anyType<T>(data: T): T {
    return data;
}
 
var str = anyType<string>("Hello world");
// console.log(str);
 
var num = anyType<number>(1);
// console.log(num);
 
var arr = anyType<Array<string>>(["Iparra", "Juan", "Manuel"]);
// console.log(arr.join("|||"));