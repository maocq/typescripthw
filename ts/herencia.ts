/**
  * HERENCIA
  */

class Vehicle {
    wheels:number;
    fuel: string;
    private color: string;
    constructor(wheels: number, fuel: string, color: string = 'White') {
        this.wheels = wheels;
        this.fuel = fuel;
        this.color = color;
    }
}
 
class Car extends Vehicle {
    constructor() {
        super(1, "gasoil", 'Red');
    }
}
 
class Motorcycle extends Vehicle {
 
}
 
var car = new Car();
var motorcycle = new Motorcycle(2, 'gasoline');
 
// console.log(car);
// console.log(motorcycle);