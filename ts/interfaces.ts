/**
  * INTERFACES
  */

interface IUser{
    name: string;
    getName(): string;
}

class UserImp implements IUser{
    name: string;
    constructor(name: string) {
        this.name= name;
    }
    getName(): string{
        return this.name;
    }    
}
var userImp = new UserImp("iparra");
// console.log(userImp);