/**
  * EXPRESIONES LAMBDA
  */
var station = {
    names: ["Piera", "Martorell", "Manresa", "Igualada", "Cornellá"],
    randomStation: function()
    {
        //esta es la clave
        return() => {
            // console.log(this);
            var rand = this.names[Math.floor(Math.random() * this.names.length)];
            return { random : rand };
        }
    }
}
var newStation = station.randomStation()();
// console.log("Station: " + newStation.random);