/**
  * MODULOS
  */

module Validator{
    export class EmailValid{
        isValid(email: string): boolean{
            var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            return re.test(email);
        }
    }
}

// Import modulo
import EmailValid = Validator.EmailValid;

// console.log(new EmailValid().isValid("iparra@mail.com"));