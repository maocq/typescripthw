/**
  * TIPOS
  */

/**
 * Booleans
 */
var bool: boolean = false;

/**
 * Strings
 */
var name: string = "Iparra";

/**
 * Number
 */
var age: number = 34;

/**
 * Array
 */
var hobbies: Array<string> = ["Family", "Program"];

/**
 * Enum
 */
enum Color {Red, Green, Blue};
var c: Color = Color.Blue; // 2

/**
 * Any
 */
var any: any = "any";
    any = 23;
    any = false;

/**
 * Void
 */
function setId(id: number): void{
    console.log(id);
}

/**
 * Funciones
 */
function getId(): number{
    return 1;
}