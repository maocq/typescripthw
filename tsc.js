var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
/**
  * HOLA MUNDO
  */
var helloWord = "Iparra";
// console.log(helloWord); 
/**
  * TIPOS
  */
/**
 * Booleans
 */
var bool = false;
/**
 * Strings
 */
var name = "Iparra";
/**
 * Number
 */
var age = 34;
/**
 * Array
 */
var hobbies = ["Family", "Program"];
/**
 * Enum
 */
var Color;
(function (Color) {
    Color[Color["Red"] = 0] = "Red";
    Color[Color["Green"] = 1] = "Green";
    Color[Color["Blue"] = 2] = "Blue";
})(Color || (Color = {}));
;
var c = Color.Blue; // 2
/**
 * Any
 */
var any = "any";
any = 23;
any = false;
/**
 * Void
 */
function setId(id) {
    console.log(id);
}
/**
 * Funciones
 */
function getId() {
    return 1;
}
/**
  * CLASES
  */
var User = (function () {
    function User(id, name, email, hobbies) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.hobbies = hobbies;
    }
    User.prototype.getName = function () {
        return this.name;
    };
    return User;
}());
var user = new User(1, "iparra", "iparra@mail.com", ["Family", "Program"]);
// console.log(user.getName()); 
/**
  * HERENCIA
  */
var Vehicle = (function () {
    function Vehicle(wheels, fuel, color) {
        if (color === void 0) { color = 'White'; }
        this.wheels = wheels;
        this.fuel = fuel;
        this.color = color;
    }
    return Vehicle;
}());
var Car = (function (_super) {
    __extends(Car, _super);
    function Car() {
        _super.call(this, 1, "gasoil", 'Red');
    }
    return Car;
}(Vehicle));
var Motorcycle = (function (_super) {
    __extends(Motorcycle, _super);
    function Motorcycle() {
        _super.apply(this, arguments);
    }
    return Motorcycle;
}(Vehicle));
var car = new Car();
var motorcycle = new Motorcycle(2, 'gasoline');
// console.log(car);
// console.log(motorcycle); 
/**
  * INTERFACES
  */
var UserImp = (function () {
    function UserImp(name) {
        this.name = name;
    }
    UserImp.prototype.getName = function () {
        return this.name;
    };
    return UserImp;
}());
var userImp = new UserImp("iparra");
// console.log(userImp); 
/**
  * MODULOS
  */
var Validator;
(function (Validator) {
    var EmailValid = (function () {
        function EmailValid() {
        }
        EmailValid.prototype.isValid = function (email) {
            var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            return re.test(email);
        };
        return EmailValid;
    }());
    Validator.EmailValid = EmailValid;
})(Validator || (Validator = {}));
// Import modulo
var EmailValid = Validator.EmailValid;
// console.log(new EmailValid().isValid("iparra@mail.com")); 
/**
  * FUNCIONES
  */
function vehicleComponents(wheels, fuel, color) {
    if (color) {
        return "Wheels: " + wheels + ", fuel: " + fuel + ", color: " + color;
    }
    else {
        return "Wheels: " + wheels + ", fuel: " + fuel;
    }
}
var vehicleWhitColor = vehicleComponents(4, 'oil', 'red');
var vehicleWhitouthColor = vehicleComponents(4, 'oil');
// console.log(vehicleWhitColor);
// console.log(vehicleWhitouthColor); 
/**
  * EXPRESIONES LAMBDA
  */
var station = {
    names: ["Piera", "Martorell", "Manresa", "Igualada", "Cornellá"],
    randomStation: function () {
        var _this = this;
        //esta es la clave
        return function () {
            // console.log(this);
            var rand = _this.names[Math.floor(Math.random() * _this.names.length)];
            return { random: rand };
        };
    }
};
var newStation = station.randomStation()();
// console.log("Station: " + newStation.random); 
/**
  * GENÉRICOS
  */
function anyType(data) {
    return data;
}
var str = anyType("Hello world");
// console.log(str);
var num = anyType(1);
// console.log(num);
var arr = anyType(["Iparra", "Juan", "Manuel"]);
// console.log(arr.join("|||")); 
//# sourceMappingURL=tsc.js.map